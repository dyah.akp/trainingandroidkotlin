package com.example.projectdyahbsit

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.projectdyahbsit.LoginActivity.Companion.KEY_ADDRESS
import com.example.projectdyahbsit.LoginActivity.Companion.KEY_INPUT
import com.example.projectdyahbsit.LoginActivity.Companion.KEY_NAME
import com.example.projectdyahbsit.databinding.ActivityMenuBinding
import com.example.projectdyahbsit.databinding.ActivityProfileBinding

class ProfileActivity : AppCompatActivity() {
    private lateinit var binding: ActivityProfileBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityProfileBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val name = intent.getStringExtra(KEY_NAME)
        val address = intent.getStringExtra(KEY_ADDRESS)
        val valueInput = intent.getStringExtra(KEY_INPUT)
        binding.tvName.text = valueInput

    }
}