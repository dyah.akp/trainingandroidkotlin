package com.example.projectdyahbsit

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.projectdyahbsit.databinding.ActivityBiodataBinding

class BiodataActivity : AppCompatActivity() {
    private lateinit var binding: ActivityBiodataBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityBiodataBinding.inflate(layoutInflater)
        setContentView(binding.root)
        val names = "Dyah Anggraini "
        val ages = 22
        val genders = "Female"
        val emails = "dyah.akp@gmail.com"
        val telp = "089528941578"
        val majors = "Information Systems"
        val university = "UPN Veteran Yogyakarta"
        val addresses = "Klaten, Central of Java"

        binding.name.text = names
        binding.age.text = ages.toString()
        binding.gender.text = genders
        binding.email.text = emails
        binding.phoneNumber.text = telp
        binding.major.text = majors
        binding.univ.text = university
        binding.address.text = addresses



        //var name="Dyah Anggraini Kartika Putri"
        // Toast.makeText(applicationContext, name, Toast.LENGTH_LONG).show()
    }
}