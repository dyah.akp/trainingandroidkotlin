package com.example.projectdyahbsit


import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.example.projectdyahbsit.databinding.ActivityCalculatorBinding

class CalculatorActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCalculatorBinding

    private var input1 = 0
    private var input2 = 0
    private var inputResult = 0
    private var operatorType = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCalculatorBinding.inflate(layoutInflater)
        setContentView(binding.root)
        binding.btnTambah.setOnClickListener {
            changeOperator("+")
            operatorType = OperatorType.TAMBAH
        }
        binding.btnKurang.setOnClickListener {
            changeOperator("-")
            operatorType = OperatorType.KURANG
        }
        binding.btnKali.setOnClickListener {
            changeOperator("x")
            operatorType = OperatorType.KALI
        }
        binding.btnBagi.setOnClickListener {
            changeOperator("/")
            operatorType = OperatorType.BAGI
        }
        binding.btnResult.setOnClickListener {
            calculator()
        }

    }

    private fun calculator() {
        input1 = binding.etInput1
            .text // mengambil teks dari edit text
            .toString() // merubah text ke string
            .toInt() //merubah string ke integer

        input2 = binding.etInput2
            .text // mengambil teks dari edit text
            .toString() // merubah text ke string
            .toInt() //merubah string ke integer
        when (operatorType) {
            OperatorType.TAMBAH -> {
                //penambahan dijalankan
                tambah()
            }
            OperatorType.KURANG -> {
                //pengurangan dijalankan
                kurang()
            }
            OperatorType.KALI -> {
                //perkalian dijalankan
                kali()
            }
            OperatorType.BAGI -> {
                //pembagian dijalankan
                bagi()
            }
        }
        binding.tvResult.text = inputResult.toString()
    }

    private fun changeOperator(operator: String) {
        binding.tvOperator.text = operator
    }

    private fun tambah() {
        inputResult = input1 + input2
    }

    private fun kurang() {
        inputResult = input1 - input2
    }

    private fun kali() {
        inputResult = input1 * input2
    }

    private fun bagi() {
        inputResult = input1 / input2
    }
}