package com.example.projectdyahbsit

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.projectdyahbsit.databinding.ActivityMenuBinding

class HomeActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMenuBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMenuBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.menu1.setOnClickListener {
            //application context = this (konten atau halaman saat ini)
            //intent perpindsahan activity 1 ke activity lain
            val intent = Intent (applicationContext, BiodataActivity::class.java)
            startActivity(intent)
//            startActivity(Intent(applicationContext, BiodataActivity::class.java))
        }
        binding.menu2.setOnClickListener {
            //application context = this (konten atau halaman saat ini)
            //intent perpindsahan activity 1 ke activity lain
            val intent = Intent (applicationContext, CalculatorActivity::class.java)
            startActivity(intent)
//            startActivity(Intent(applicationContext, BiodataActivity::class.java))
        }
        binding.menu3.setOnClickListener {
            //application context = this (konten atau halaman saat ini)
            //intent perpindsahan activity 1 ke activity lain
            val intent = Intent (applicationContext, LoginActivity::class.java)
            startActivity(intent)
//            startActivity(Intent(applicationContext, BiodataActivity::class.java))
        }
    }
}