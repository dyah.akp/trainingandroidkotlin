package com.example.projectdyahbsit

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.projectdyahbsit.databinding.ActivityCalculatorBinding
import com.example.projectdyahbsit.databinding.ActivityLoginBinding

class LoginActivity : AppCompatActivity() {
    val email = "dyah.akp@gmail.com"
    val name = "Dyah Anggraini Kartika Putri"
    val address = "Griya Prima Timur No.476 Rt.06/Rw.18, Klaten, Central Java"
    private lateinit var binding: ActivityLoginBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityLoginBinding.inflate(layoutInflater)
        setContentView(binding.root)

        //biar inputnya bukan karakter tp string
        binding.btnSubmit.setOnClickListener {
            val inputEmail = binding.etEmail.text.toString()
            navigateScreenWithInput(ProfileActivity::class.java, inputEmail)
//            if (email == "dyah.akp@gmail.com") {
//                Toast.makeText(applicationContext, "Login Successfull!", Toast.LENGTH_LONG).show()
//                val intent = Intent (applicationContext, BiodataActivity::class.java)
//                startActivity(intent)
//                navigateScreen(ProfileActivity::class.java)
//
//            } else {
//                Toast.makeText(applicationContext, "Login Failed!", Toast.LENGTH_LONG).show()
//            }

            showLoading()
        }
        binding.pbLoading.setOnClickListener {
            hidingLoading()
        }

    }

    private fun showLoading() {
        binding.pbLoading.visibility = View.VISIBLE
        binding.btnSubmit.visibility = View.GONE

    }

    private fun hidingLoading() {
        binding.pbLoading.visibility = View.GONE
        binding.btnSubmit.visibility = View.VISIBLE

    }

    private fun navigateScreen(screen: Class<*>) {
        val intent = Intent(applicationContext, screen)
        //companion object : konstant atau static
        intent.putExtra(KEY_NAME, name)
        intent.putExtra(KEY_ADDRESS, address)
        startActivity(intent)

    }

    private fun navigateScreenWithInput(screen: Class<*>, input: String) {
        val intent = Intent(applicationContext, screen)
        //companion object : konstant atau static
        intent.putExtra(KEY_INPUT, input)
        startActivity(intent)
    }

    companion object {
        const val KEY_NAME = "name"
        const val KEY_ADDRESS = "address"
        const val KEY_INPUT = "input"

    }
}